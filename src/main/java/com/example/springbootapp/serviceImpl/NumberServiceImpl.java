package com.example.springbootapp.serviceImpl;

import com.example.springbootapp.dao.NumberDao;
import com.example.springbootapp.entity.NumberValueEntity;
import com.example.springbootapp.service.NumberService;
import com.example.springbootapp.service.model.NumberValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;

import java.util.List;

@Service
public class NumberServiceImpl implements NumberService {
    @Autowired
    private NumberDao numberDao;


    @Override
    public NumberValue save(@NotNull NumberValue number) {
        NumberValueEntity entity = new NumberValueEntity();

        entity.setTvalue(number.getValue());
        entity.setDescription(number.getDescription());

        entity = numberDao.save(entity);

        number.setDescription(entity.getDescription());
        number.setId(entity.getId());
        number.setValue(entity.getTvalue());
        return number;
    }


    @Override
    public NumberValue findOne(@NotNull Long id) {
//        return numberDao.findOne(id);
        return null;

    }

    @Override
    public void delete(@NotNull Long id) {
        numberDao.deleteById(id);
    }

    @Override
    public void update(@NotNull NumberValue number) {
        NumberValueEntity numberBd = numberDao.getOne(number.getId());
        numberBd.setTvalue(number.getValue());
        numberDao.saveAndFlush(numberBd);
    }

    @Override
    public List<NumberValueEntity> findAll() {
      return (List<NumberValueEntity>) numberDao.findAll();
    }

    //2 3 5 7 11 13 ...
    // 2 / 2 = 1
    // 2 / 1 = 2

    // Main -> Conroller(View, Model)
    // Controller -> userProcess(); View Model
    // Scanner read, -> read -> 10

    // n is prime ?
    // sqrt(n) -> sn
    // i = 2
    // while sn % i == 0 -> is

    // boolean isPrime(long int short byte)

    // [0, 1000]
    // int theNumber = random(0, 1000);
    //
    // application guessed number you should try ...
    // 50 -> less or more
    // binary search - > [1,2,3,4,5,6,7} -> find 3 -> [1,2,3,4] , [5,6,7] -> log(n)


    // prime

    // View -> System.out.println
    // Model


    @Override
    public int changeNumberValue(@NotNull Long newNumber, @NotNull Long oldNumber) {
        return numberDao.updateOldValueToNewValues(newNumber, oldNumber);
    }
}
