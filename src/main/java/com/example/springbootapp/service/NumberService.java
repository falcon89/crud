package com.example.springbootapp.service;


import com.example.springbootapp.entity.NumberValueEntity;
import com.example.springbootapp.service.model.NumberValue;
//import org.thymeleaf.expression.Numbers;

import java.util.List;

public interface NumberService {

    NumberValue save(NumberValue numberValue);

    NumberValue findOne(Long id);

    void delete(Long id);

    void update(NumberValue number);

    List<NumberValueEntity> findAll();

    int changeNumberValue (Long newNumber , Long oldNumber);
}
