package com.example.springbootapp.service.model;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@EqualsAndHashCode
public class NumberValue {
    private Long id;
    private Long value;
    private String description;

    public NumberValue(Long id, Long value) {
        this.id = id;
        this.value = value;
    }

    //    public NumberValue() {
//    }
//
//    public NumberValue(Long id, Long t_value, String description) {
//        this.id = id;
//        this.t_value = t_value;
//        this.description = description;
//    }
}
