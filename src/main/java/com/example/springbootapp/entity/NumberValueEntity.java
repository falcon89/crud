package com.example.springbootapp.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "T_value")
@ToString
public class NumberValueEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "Tvalue")
    private Long Tvalue;

    @Column(name = "description")
    private String description;
}
