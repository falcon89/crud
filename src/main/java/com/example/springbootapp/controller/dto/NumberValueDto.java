package com.example.springbootapp.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class NumberValueDto {
    private Long id;

    @NotNull
    private Long number;

    @NotEmpty
    @Size(max = 20, min = 1)
    private String description;
}
