package com.example.springbootapp.controller;

import com.example.springbootapp.controller.dto.NumberValueDto;

import com.example.springbootapp.entity.NumberValueEntity;
import com.example.springbootapp.service.NumberService;
import com.example.springbootapp.service.model.NumberValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.constraints.NotNull;

@Controller
//@RequestMapping("/numbers")
public class IndexController {

    @Autowired
    private NumberService numberService;

    @GetMapping({"/", "/index"})
    public String Index(Model model) {
        model.addAttribute("valueses", numberService.findAll());
        return "index";
    }

    @GetMapping({"/index/{id}"})
    public ModelAndView numbers(@PathVariable Long id, ModelAndView modelAndView) {
        NumberValue value = numberService.findOne(id);
        if (value == null) {
            modelAndView.setViewName("404");
        } else {
            modelAndView.setViewName("index");
            modelAndView.addObject("number", value);
        }
        return modelAndView;
    }


    @PostMapping("/numbersNew")
    public ResponseEntity<Integer> updateNewNumber(@NotNull @RequestParam("oldNumber") Long oldNumber, @NotNull @RequestParam("newNumber") Long newNumber){
        return ResponseEntity.ok(numberService.changeNumberValue(newNumber, oldNumber));
    }


    @PostMapping("/deleteNumber/{id}")
    public String deleteNumber(@PathVariable Long id) {
        numberService.delete(id);
        return "redirect:/index";
    }

    @PostMapping("/numbers")
    public ResponseEntity<NumberValueDto> createNumber(@NotNull NumberValueDto dto) {
        // validation
        NumberValue value = new NumberValue();

        value.setValue(dto.getNumber());
        value.setDescription(dto.getDescription());

        value = numberService.save(value);

        dto.setDescription(value.getDescription());
        dto.setId(value.getId());
        dto.setNumber(value.getValue());

        return ResponseEntity.ok(dto);
    }

    @PostMapping("/updateNumber/{id}")
    public String updateNumber(@PathVariable("id") long id, @RequestParam("number") long number) {
        numberService.update(new NumberValue(id, number));
        return "redirect:/index";
    }


}
