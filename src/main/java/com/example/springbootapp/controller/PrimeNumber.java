package com.example.springbootapp.controller;

import org.hibernate.criterion.Order;
import sun.applet.Main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class PrimeNumber {
    public static void main(String[] args) {
//        HashSet<String> words = new HashSet<>(100);
//        long callTime = System.nanoTime();
//        Scanner scan = null;
//        try {
//            scan = new Scanner(new File("texts\\test.txt"));
//            scan.useDelimiter("[^А-я]+");
//            while (scan.hasNext()) {
//                String word = scan.next();
//                words.add(word.toLowerCase());
//            }
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//        Iterator<String> it = words.iterator();
//        while (it.hasNext()) {
//            System.out.println(it.next());
//        }
//        TreeSet<String> ts = new TreeSet<>(words);
//        System.out.println(ts);
//        long totalTime = System.nanoTime() - callTime;
//        System.out.println("различных слов: " + words.size() + ", "
//                + totalTime + " наносекунд");

//
        Integer temp;
        boolean isPrime=true;
        Scanner scan= new Scanner(System.in);
        System.out.println("Enter number:");
        Integer num=scan.nextInt();
        scan.close();
        for(Integer i=2;i<=num/2;i++)
        {
            temp=num%i;
            if(temp==0)
            {
                isPrime=false;
                break;
            }
        }
        if(isPrime)
            System.out.println(num + " is a Prime Number");
        else
            System.out.println(num + " is not a Prime Number");
    }
}
