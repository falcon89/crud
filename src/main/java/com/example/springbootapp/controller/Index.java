package com.example.springbootapp.controller;

import com.example.springbootapp.dao.NumberDao;
import com.example.springbootapp.entity.Number;
import com.example.springbootapp.service.NumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class Index {

    @Autowired
    private NumberService numberService;

    @Autowired
    private NumberDao numberDao;

    @GetMapping({"/","/index"})
    public String Index(Model model) {
        model.addAttribute("numberses", numberService.findAll());
        return "index";
    }

    @GetMapping("/deleteNumber/{id}")
    public String deleteNumber(@PathVariable long id) {
        numberService.delete(id);
        return "redirect:/index";
    }

    @PostMapping("/updateNumber/{id}")
    public String updateNumber(@PathVariable long id,
                               @RequestParam int number) {
//        numberService.put(new Number(id, number));
        return "redirect:/index";
    }


}
