package com.example.springbootapp.dao;

import com.example.springbootapp.entity.NumberValueEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface NumberDao extends JpaRepository<NumberValueEntity, Long> {

    @Modifying
    @Transactional
    @Query(value = "UPDATE t_value SET t_value.tvalue =:newNumber WHERE t_value.tvalue =:oldNumber", nativeQuery = true)
    int updateOldValueToNewValues(@Param("newNumber") Long n, @Param("oldNumber") Long o);

}